import {Component, OnInit} from '@angular/core';
import {AppError, ErrorHandlerService} from "./services/ErrorHandlerService";
import {UserService} from "./services/UserService";
import {UIService} from "./modules/ui/services/UIService";

@Component({
  selector: 'app-root',
  templateUrl: './app.html',
})
export class App implements OnInit {
  title = 'chat-client';

  public constructor(private errorHandlerService: ErrorHandlerService, private userService: UserService,
                     private uiService: UIService) {

  }

  ngOnInit(): void {
    this.errorHandlerService.lastError.subscribe({
      next: (err: AppError) => {
        if (err) {
          switch (err.name) {
            case "TOKEN_EXPIRED":
              this.uiService.showAlert("Error", "Token expired, please login again")
                .then(() => {
                  this.userService.logout();
                });
              break;
            default:
              this.uiService.showAlert("Error", err.name + (err.message ? ": " + err.message : ""));
          }
        }
      }
    });
  }

}
