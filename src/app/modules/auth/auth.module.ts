import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {routes} from "./auth.routing";
import {LoginPage} from "./pages/login/login.page";
import {RegisterPage} from "./pages/register/register.page";
import {AuthService} from "./services/AuthService";

@NgModule({
  declarations: [
    RegisterPage,
    LoginPage,
  ],
  imports: [
    FormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    AuthService,
  ]
})
export class AuthModule {

}
