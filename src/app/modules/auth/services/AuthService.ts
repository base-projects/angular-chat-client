import {Injectable} from "@angular/core";
import * as API from "../../../../../../claire-cli/lib/template/export-1.0.0";
import {ApiService} from "../../../services/ApiService";

@Injectable()
export class AuthService {

  public constructor(private apiService: ApiService) {

  }

  public async login(username: string, password: string): Promise<API.AuthAPI.response.Login> {
    let params: API.AuthAPI.request.Login = {
      username, password
    };
    return await this.apiService.callJsonApi<API.AuthAPI.request.Login, API.AuthAPI.response.Login>(API.AuthAPI.url.Login, params);
  }

  public async registerAccount(username: string, password: string): Promise<API.AuthAPI.response.RegisterAccount> {
    let params: API.AuthAPI.request.RegisterAccount = {
      username, password
    };
    return await this.apiService.callJsonApi<API.AuthAPI.request.RegisterAccount, API.AuthAPI.response.RegisterAccount>(API.AuthAPI.url.RegisterAccount, params);
  }

}
