import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {UIService} from "../../../ui/services/UIService";
import {AuthService} from "../../services/AuthService";
import {UserService} from "../../../../services/UserService";
import {AppError, ErrorHandlerService} from "../../../../services/ErrorHandlerService";

@Component({
  selector: "login-page",
  templateUrl: "login.page.html",
})
export class LoginPage {

  username: string;
  password: string;

  public constructor(private uiService: UIService, private errorHandlerService: ErrorHandlerService,
                     private authService: AuthService, private userService: UserService,
                     private router: Router) {

  }

  login() {
    (async () => {
      let dialog = this.uiService.showBlockingProgress("Login in progress...");
      let loginResult = await this.authService.login(this.username, this.password);
      this.userService.updateUserBasicInfo(loginResult.userId);
      this.userService.updateUserLoginInfo(loginResult.accessToken);
      dialog.updateMessage("Loading info...");
      let userInfo = await this.userService.getMyInfo();
      this.userService.updateUserBasicInfo(undefined, userInfo.username, userInfo.avatarUrl);
      dialog.hide();
      //--
      this.router.navigate(["/chat"]);
    })()
      .catch((err: AppError) => {
        this.errorHandlerService.handle(err);
      });
  }


}
