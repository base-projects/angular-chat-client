import {Component} from "@angular/core";
import {UIService} from "../../../ui/services/UIService";
import {AuthService} from "../../services/AuthService";
import {ErrorHandlerService} from "../../../../services/ErrorHandlerService";
import {UserService} from "../../../../services/UserService";
import {Router} from "@angular/router";

@Component({
  selector: "register-page",
  templateUrl: "register.page.html",
})
export class RegisterPage {

  username: string;

  password: string;

  passwordRepeat: string;

  public constructor(private uiService: UIService, private errorHandlerService: ErrorHandlerService,
                     private authService: AuthService, private userService: UserService, private router: Router) {
  }

  register() {
    (async () => {
      let dialog = this.uiService.showBlockingProgress("Creating your account...");
      let registerResult = await this.authService.registerAccount(this.username, this.password);
      this.userService.updateUserBasicInfo(registerResult.userId, this.username, registerResult.avatarUrl);
      dialog.updateMessage("Logging in into your account...");
      let loginResult = await this.authService.login(this.username, this.password);
      this.userService.updateUserLoginInfo(loginResult.accessToken);
      dialog.hide();
      //-- redirect to main page
      this.router.navigate(["/chat"]);
    })()
      .catch((err) => {
        this.errorHandlerService.handle(err);
      });
  }
}
