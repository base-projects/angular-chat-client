import {Component} from "@angular/core";
import {Subject} from "rxjs";

@Component({
  selector: "add-channel",
  templateUrl: "./add-channel.dialog.html",
})
export class AddChannelDialog {

  channelName: string;
  channelPassword: string;

  public resolver: Subject<any> = new Subject();

  createChannel() {
    this.resolver.next({channelName: this.channelName, channelPassword: this.channelPassword});
  }

  close() {
    this.resolver.next();
  }
}

