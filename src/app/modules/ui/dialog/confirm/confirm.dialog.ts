import {Component, Input} from "@angular/core";
import {Subject} from "rxjs";

@Component({
  templateUrl: "./confirm.dialog.html",
})
export class ConfirmDialog {

  @Input()
  title: string;

  @Input()
  message: string;

  @Input()
  showCancel: boolean = true;

  public resolver: Subject<boolean> = new Subject();

  close() {
    this.resolver.next(false);
  }

  confirm() {
    this.resolver.next(true);
  }

}
