import {Component, Input} from "@angular/core";
import {Subject} from "rxjs";

@Component({
  templateUrl: "./input-password.dialog.html",
})
export class InputPasswordDialog {

  @Input()
  title: string;

  @Input()
  placeholder: string;

  @Input()
  allowEmpty: boolean;

  @Input()
  okTitle: string;

  channelPassword: string;

  public resolver: Subject<string> = new Subject();

  close() {
    this.resolver.next();
  }

  joinChannel() {
    this.resolver.next(this.channelPassword);
  }

}
