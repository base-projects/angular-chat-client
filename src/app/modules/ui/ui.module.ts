import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {ToggleButtonComponent} from "./components/toggle/toggle.component";
import {AddChannelDialog} from "./dialog/add-channel/add-channel.dialog";
import {UIService} from "./services/UIService";
import {NgbModalModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {InputPasswordDialog} from "./dialog/input-password/input-password.dialog";
import {ConfirmDialog} from "./dialog/confirm/confirm.dialog";

@NgModule({
  declarations: [
    ToggleButtonComponent,
    AddChannelDialog,
    InputPasswordDialog,
    ConfirmDialog,
  ],
  imports: [
    CommonModule,
    NgbModalModule,
    FormsModule,
  ],
  exports: [
    ToggleButtonComponent,
  ],
  providers: [
    UIService,
  ],
  entryComponents: [
    AddChannelDialog,
    InputPasswordDialog,
    ConfirmDialog,
  ]
})
export class UIModule {

}
