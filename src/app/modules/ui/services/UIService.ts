import {Injectable} from "@angular/core";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AddChannelDialog} from "../dialog/add-channel/add-channel.dialog";
import {InputPasswordDialog} from "../dialog/input-password/input-password.dialog";
import {ConfirmDialog} from "../dialog/confirm/confirm.dialog";

@Injectable()
export class UIService {

  public constructor(private ngbModal: NgbModal) {

  }

  public showAddChannelDialog(): Promise<any> {
    let modal = this.ngbModal.open(AddChannelDialog);
    modal.componentInstance.resolver.subscribe({
      next: (data) => {
        modal.close(data);
      }
    });
    return modal.result;
  }

  public showAlert(title: string, message: string){
    let modal = this.ngbModal.open(ConfirmDialog);
    modal.componentInstance.title = title;
    modal.componentInstance.message = message;
    modal.componentInstance.showCancel = false;
    modal.componentInstance.resolver.subscribe({
      next: (data) => {
        modal.close(data);
      }
    });
    return modal.result;
  }

  public showConfirmDialog(title: string, message: string){
    let modal = this.ngbModal.open(ConfirmDialog);
    modal.componentInstance.title = title;
    modal.componentInstance.message = message;
    modal.componentInstance.resolver.subscribe({
      next: (data) => {
        modal.close(data);
      }
    });
    return modal.result;
  }

  public showInputPasswordDialog(title: string, allowEmpty: boolean, placeholder: string, okTitle: string) {
    let modal = this.ngbModal.open(InputPasswordDialog);
    modal.componentInstance.title = title;
    modal.componentInstance.allowEmpty = allowEmpty;
    modal.componentInstance.placeholder = placeholder;
    modal.componentInstance.okTitle = okTitle;
    modal.componentInstance.resolver.subscribe({
      next: (data) => {
        modal.close(data);
      }
    });
    return modal.result;
  }

  public showBlockingProgress(message: string): BlockingDialog {
    return new BlockingDialog(message);
  }

}


export class BlockingDialog {

  private message: string;

  public constructor(message: string) {
    this.message = message;
  }

  public hide(): void {

  }

  public updateMessage(message: string) {
    this.message = message;
  }
}
