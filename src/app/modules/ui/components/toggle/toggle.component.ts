import {Component, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'toggle-button',
  templateUrl: "./toggle.component.html",
  styleUrls: ["./toggle.component.scss"],
})
export class ToggleButtonComponent {

  @Input()
  selected: boolean;

}
