import {Component, OnInit} from "@angular/core";
import {ChatUserService} from "../../../../services/ChatUserService";
import {SocketService} from "../../../../services/SocketService";
import {ErrorHandlerService} from "../../../../../../services/ErrorHandlerService";
import {environment} from "../../../../../../../environments/environment";

@Component({
  selector: "user-list",
  templateUrl: "./user-list.partial.html",
})

export class UserListPartial implements OnInit {

  private serverUrl = environment.API_SERVER_URL;
  isShowAllUsers: boolean;
  users: { username: string, isOnline: boolean, avatarUrl: string, userId: number }[];

  public constructor(protected chatUserService: ChatUserService, private socketService: SocketService, private errorHandlerService: ErrorHandlerService) {

  }

  ngOnInit(): void {

    this.isShowAllUsers = false;

    this.chatUserService.usersSubject.subscribe({
      next: (users) => {
        this.users = users.map((u) => ({
          username: u.username,
          isOnline: this.chatUserService.onlineUsersSubject.getValue().indexOf(u.userId) >= 0,
          avatarUrl: u.avatarUrl,
          userId: u.userId,
        }));
      }
    });

    this.chatUserService.onlineUsersSubject.subscribe({
      next: (userIds) => {
        this.users.forEach((u) => {
          u.isOnline = userIds.indexOf(u.userId) >= 0;
        });
      }
    });

  }

}
