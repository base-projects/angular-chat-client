import {Component, OnInit} from "@angular/core";
import {SocketService} from "../../../../services/SocketService";
import {ChatUserService} from "../../../../services/ChatUserService";
import * as moment from "moment";
import {Channel} from "../../../../models/Channel";
import {AppError, ErrorHandlerService} from "../../../../../../services/ErrorHandlerService";
import {User} from "../../../../models/User";
import {environment} from "../../../../../../../environments/environment";
import {UserService} from "../../../../../../services/UserService";
import {UIService} from "../../../../../ui/services/UIService";
import {Message} from "../../../../models/Message";
import {MessageType} from '../../../../../../../../../claire-cli/lib/template/export-1.0.0';

@Component({
  selector: "private-message",
  templateUrl: "./pm.partial.html",
  styleUrls: ["./pm.partial.scss"],
})
export class PMPartial implements OnInit {
  public constructor(private socketService: SocketService, private chatUserService: ChatUserService, private errorHandlerService: ErrorHandlerService,
                     private userService: UserService, private uiService: UIService) {

  }

  serverUrl = environment.API_SERVER_URL;
  currentChannel: Channel | undefined;
  currentChannelUsers: User[];

  messages: {
    messageId: string;
    username: string;
    datetime: string;
    content: string;
    type: MessageType,
    userAvatar: string;
  }[] = [];

  ngOnInit(): void {

    this.messages = [];

    this.chatUserService.currentJoinedChannel.subscribe({
      next: (channel: Channel | undefined) => {
        this.currentChannel = channel;
        if (this.currentChannel) {
          //-- fetch messages
          this.chatUserService.loadMessageOfCurrentChannel(0, 100)
            .catch((err: AppError) => {
              this.errorHandlerService.handle(err);
            });
          this.chatUserService.loadJoinUserOfCurrentChannel()
            .catch((err: AppError) => {
              this.errorHandlerService.handle(err);
            });
        }
      }
    });

    this.chatUserService.currentChannelUsersSubject.subscribe({
      next: (userIds) => {
        this.currentChannelUsers = this.chatUserService.usersSubject.value.filter((u) => userIds.indexOf(u.userId) >= 0);
      }
    });

    this.chatUserService.messagesSubject.subscribe({
      next: (messages) => {
        this.messages = messages.map((m) => {
          let user = this.chatUserService.usersSubject.value.find((u) => u.userId === m.senderId);
          return {
            messageId: m.messageId,
            username: user && user.username,
            content: m.content,
            datetime: moment(m.timestamp).fromNow(),
            userAvatar: user && (this.serverUrl + user.avatarUrl),
            type: m.messageType,
          };
        });
      }
    });

  }

  changePassword() {
    this.uiService.showInputPasswordDialog("Reset channel password", true, "leave empty to remove password", "Reset")
      .then((password) => {
        this.chatUserService.updateChannelPassword(this.currentChannel.channelId, password)
          .catch((err: AppError) => {
            this.errorHandlerService.handle(err);
          });
      });
  }

  removeChannel() {
    this.uiService.showConfirmDialog("Remove channel", "Remove this channel, disconnect people and delete all messages?")
      .then((confirm) => {
        if (confirm) {
          this.chatUserService.removeChannel(this.currentChannel.channelId)
            .catch((err: AppError) => {
              this.errorHandlerService.handle(err);
            })
        }
      });
  }

  isShowConfig() {
    return !!this.currentChannel && this.currentChannel.creatorId && this.currentChannel.creatorId === this.userService.userInfo.userId;
  }

  leaveCurrentChannel() {
    this.chatUserService.leaveCurrentChannel()
      .catch((err: AppError) => {
        this.errorHandlerService.handle(err);
      });
  }

  sendMessageToChannel(message: Message) {
    if (this.currentChannel) {
      this.socketService.sendMessageToChannel(this.currentChannel.channelId, message);
    }
  }

}
