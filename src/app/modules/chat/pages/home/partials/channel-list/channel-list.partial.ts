import {Component, OnInit} from "@angular/core";
import {SocketService} from "../../../../services/SocketService";
import {ChatUserService} from "../../../../services/ChatUserService";
import {Channel} from "../../../../models/Channel";
import {UIService} from "../../../../../ui/services/UIService";
import {AppError, ErrorHandlerService} from "../../../../../../services/ErrorHandlerService";
import {UserService} from "../../../../../../services/UserService";
import {environment} from "../../../../../../../environments/environment";

@Component({
  selector: "open-channel",
  templateUrl: "./channel-list.partial.html",
})
export class ChannelListPartial implements OnInit {

  private channels: {
    channelId: number;
    isPassword: boolean;
    channelName: string;
    ownerAvatar?: string;
    ownerName?: string;
  }[];

  private currentChannel: Channel | undefined;

  public constructor(private socketService: SocketService, private chatUserService: ChatUserService, private uiService: UIService,
                     private errorHandlerService: ErrorHandlerService, private userService: UserService) {

  }

  ngOnInit(): void {


    this.chatUserService.currentJoinedChannel.subscribe({
      next: (channel) => {
        this.currentChannel = channel;
      }
    });

    this.chatUserService.channelsSubject.subscribe({
      next: (channels) => {
        this.channels = channels.map((c) => {
          let owner = !!c.creatorId && this.chatUserService.usersSubject.value.find((u) => u.userId === c.creatorId);
          return {
            channelId: c.channelId,
            channelName: c.channelName,
            isPassword: c.isPassword,
            ownerAvatar: owner ? environment.API_SERVER_URL + owner.avatarUrl : "",
            ownerName: owner ? owner.username : "",
          };
        });
      }
    });
  }

  joinChannel(channelId: number) {
    let channel = this.chatUserService.channelsSubject.value.find((c) => c.channelId === channelId);
    if (channel) {
      //-- whether to show password input
      if (channel.isPassword && channel.creatorId !== this.userService.userInfo.userId) {
        this.uiService.showInputPasswordDialog("Channel is protected", false, "password", "Join")
          .then((password) => {
            if (!!password) {
              this.doJoin(channel.channelId, password)
                .catch((err: AppError) => {
                  this.errorHandlerService.handle(err);
                });
            }
          });
      } else {
        this.doJoin(channel.channelId, undefined)
          .catch((err: AppError) => {
            this.errorHandlerService.handle(err);
          });
      }
    }
  }

  private async doJoin(channelId: number, password?: string) {
    //-- if is joining other channel then disconnect first
    if (!!this.currentChannel) {
      await this.chatUserService.leaveCurrentChannel();
    }
    await this.chatUserService.joinChannel(channelId, password);
  }

  showAddChannel() {
    this.uiService.showAddChannelDialog().then((data) => {
      if (data) {
        //-- use this data please
        this.chatUserService.addChannel(data.channelName, data.channelPassword)
          .catch((err: AppError) => {
            this.errorHandlerService.handle(err);
          })
      }
    });
  }

}
