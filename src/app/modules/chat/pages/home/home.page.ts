import {environment} from "../../../../../environments/environment";
import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../../services/UserService";
import {SocketService} from "../../services/SocketService";
import {UIService} from "../../../ui/services/UIService";
import {ChatUserService} from "../../services/ChatUserService";
import {AppError, ErrorHandlerService} from "../../../../services/ErrorHandlerService";

@Component({
  selector: "home-page",
  templateUrl: "./home.page.html",
})
export class HomePage implements OnInit {

  apiServerUrl = environment.API_SERVER_URL;

  refreshQuery: number = 0;

  public constructor(protected userService: UserService, private uiService: UIService, private socketService: SocketService,
                     private chatUserService: ChatUserService, private errorHandlerService: ErrorHandlerService) {

  }

  ngOnInit(): void {

    this.chatUserService.subscribe();

    this.chatUserService.getAllUsers()
      .then(()=>{
        this.chatUserService.getOnlineUsers()
          .catch((err: AppError) => {
            this.errorHandlerService.handle(err);
          });

        this.chatUserService.getAllChannels()
          .catch((err: AppError) => {
            this.errorHandlerService.handle(err);
          });
      })
      .catch((err: AppError) => {
        this.errorHandlerService.handle(err);
      });

    let dialog = this.uiService.showBlockingProgress("Establishing realtime connection...");
    this.socketService.connect(this.userService.userInfo.accessToken);
    this.socketService.subscribe("connect", () => {
      dialog.hide();
    });
    this.socketService.subscribe("disconnect", () => {
      //-- alert and logout
      this.logout();
    });

    this.errorHandlerService.lastError.subscribe({
      next: (err: AppError) => {
        if (err) {
          switch (err.name) {
            case "TOKEN_EXPIRED":
              this.logout();
              break;
          }
        }
      }
    });
  }

  uploadAvatar(files) {
    if (files.length) {
      this.userService.updateUserAvatar(files[0])
        .then(() => {
          this.refreshQuery++;
        })
        .catch((err: AppError) => {
          this.errorHandlerService.handle(err);
        });
    }
  }

  logout() {
    this.chatUserService.leaveCurrentChannel();
    this.socketService.disconnect();
    this.userService.logout();
  }

}
