import * as API from "../../../../../../claire-cli/lib/template/export-1.0.0";
import {Injectable} from "@angular/core";
import {Socket} from "ngx-socket-io";
import {environment} from "../../../../environments/environment";
import {Message} from "../models/Message";

@Injectable()
export class SocketService {

  private subscribers: { event: string, callback: (data: string) => void }[];
  private socket: Socket;

  public constructor() {
    this.subscribers = [];
  }

  public connect(token: String): void {
    this.socket = new Socket({
      url: environment.API_SERVER_URL,
      options: {
        path: environment.API_SERVER_SOCKET_URL,
        query: {
          token,
        },
        autoConnect: false,
        // reconnection: false,
      },
    });

    this.socket.connect();

    this.socket.on("connect", () => {
      this.subscribers.forEach((sub) => {
        this.socket.on(sub.event, sub.callback);
      });
      this.subscribers = [];
    });
  }

  public subscribe(event: string, callback: (data: any) => void) {
    if (!this.socket) {
      this.subscribers.push({event, callback});
    } else {
      this.socket.on(event, callback);
    }
  }

  public disconnect(): void {
    this.socket.disconnect();
    this.socket = undefined;
  }

  public sendMessageToChannel(channelId: number, message: Message) {
    this.socket.emit(API.SocketAPI.channel.ChatMessage, {
      channelId: channelId,
      content: message.content,
      messageType: message.messageType,
    });
  }

}
