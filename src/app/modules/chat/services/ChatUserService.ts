import * as API from "../../../../../../claire-cli/lib/template/export-1.0.0";
import {Injectable} from "@angular/core";
import {ApiService} from "../../../services/ApiService";
import {User} from "../models/User";
import {UserService} from "../../../services/UserService";
import {BehaviorSubject} from "rxjs";
import {Channel} from "../models/Channel";
import {SocketService} from "./SocketService";
import {Message} from "../models/Message";

@Injectable()
export class ChatUserService {

  public usersSubject: BehaviorSubject<User[]>;
  public channelsSubject: BehaviorSubject<Channel[]>;
  public onlineUsersSubject: BehaviorSubject<number[]>;
  public messagesSubject: BehaviorSubject<Message[]>;

  public currentJoinedChannel: BehaviorSubject<Channel | undefined>;
  public currentChannelUsersSubject: BehaviorSubject<number[]>;

  public constructor(private apiService: ApiService, private userService: UserService, private socketService: SocketService) {
    this.usersSubject = new BehaviorSubject([]);
    this.channelsSubject = new BehaviorSubject([]);
    this.onlineUsersSubject = new BehaviorSubject([]);
    this.messagesSubject = new BehaviorSubject([]);

    this.currentJoinedChannel = new BehaviorSubject(undefined);
    this.currentChannelUsersSubject = new BehaviorSubject([]);
  }

  public subscribe() {

    this.socketService.subscribe(API.SocketAPI.channel.UserOnlineNotification, (data) => {
      let userId = data["userId"];
      if (this.onlineUsersSubject.value.indexOf(userId) < 0) {
        this.onlineUsersSubject.next(this.onlineUsersSubject.getValue().concat(userId));
      }
    });

    this.socketService.subscribe(API.SocketAPI.channel.UserOfflineNotification, (data) => {
      let userId = data["userId"];
      let index = this.onlineUsersSubject.value.indexOf(userId);
      if (index >= 0) {
        let oldArray = this.onlineUsersSubject.getValue();
        this.onlineUsersSubject.next([
          ...oldArray.slice(0, index),
          ...oldArray.slice(index + 1),
        ]);
      }
    });

    this.socketService.subscribe(API.SocketAPI.channel.NewChannelNotification, (data) => {
      this.channelsSubject.next(this.channelsSubject.value.concat({
        channelId: data.channelId,
        channelName: data.channelName,
        isPassword: data.isPassword,
        creatorId: data.creatorId,
      }));
    });

    this.socketService.subscribe(API.SocketAPI.channel.ChannelUpdateNotification, (data) => {
      let channelIndex = this.channelsSubject.value.findIndex((c) => c.channelId === data.channelId);
      if (channelIndex >= 0) {
        this.channelsSubject.next([
          ...this.channelsSubject.value.slice(0, channelIndex),
          Object.assign({}, this.channelsSubject.value[channelIndex], {isPassword: data.isPassword}),
          ...this.channelsSubject.value.slice(channelIndex + 1),
        ]);
      }
    });

    this.socketService.subscribe(API.SocketAPI.channel.ChannelRemovedNotification, (data) => {
      let channelIndex = this.channelsSubject.value.findIndex((c) => c.channelId === data.channelId);
      if (channelIndex >= 0) {
        this.channelsSubject.next([
          ...this.channelsSubject.value.slice(0, channelIndex),
          ...this.channelsSubject.value.slice(channelIndex + 1),
        ]);

        //-- check current join channel
        if (this.currentJoinedChannel.value && this.currentJoinedChannel.value.channelId === data.channelId) {
          this.currentJoinedChannel.next(undefined);
        }
      }
    });

    this.socketService.subscribe(API.SocketAPI.channel.UserJoinChannelNotification, (data) => {
      if (this.currentJoinedChannel.value && data.channelId === this.currentJoinedChannel.value.channelId) {
        this.currentChannelUsersSubject.next(this.currentChannelUsersSubject.value.concat(data.userId));
      }
    });

    this.socketService.subscribe(API.SocketAPI.channel.UserLeaveChannelNotification, (data) => {
      if (this.currentJoinedChannel.value && data.channelId === this.currentJoinedChannel.value.channelId) {
        let index = this.currentChannelUsersSubject.value.indexOf(data.userId);
        if (index >= 0) {
          this.currentChannelUsersSubject.next([
            ...this.currentChannelUsersSubject.value.slice(0, index),
            ...this.currentChannelUsersSubject.value.slice(index + 1),
          ]);
        }
      }
    });

    this.socketService.subscribe(API.SocketAPI.channel.ChatMessage, (data) => {
      this.messagesSubject.next(this.messagesSubject.value.concat({
        messageId: data.messageId,
        senderId: data.senderId,
        content: data.content,
        timestamp: data.timestamp,
        channelId: data.channelId,
        messageType: data.contentType,
      }));
    });
  }

  public async getOnlineUsers() {
    let data = await this.apiService.callJsonApi(API.UserAPI.url.GetOnlineUsers, undefined, this.userService.userInfo.accessToken);
    let result = data as API.UserAPI.response.GetOnlineUsers;
    this.onlineUsersSubject.next(result.userIds);
  }

  public async getAllChannels() {
    let result = await this.apiService.callJsonApi(API.ChannelAPI.url.GetAllChannels, undefined, this.userService.userInfo.accessToken);
    let channels = result as API.ChannelAPI.response.GetAllChannels;
    this.channelsSubject.next(channels.channels.map((channel) => ({
      channelId: channel.channelId,
      channelName: channel.channelName,
      isPassword: channel.isPassword,
      creatorId: channel.creatorId,
    })));
  }

  public async getAllUsers() {
    let result = await this.apiService.callJsonApi(API.UserAPI.url.GetAllUsers, undefined, this.userService.userInfo.accessToken);
    let users = result as API.UserAPI.response.GetAllUsers;
    this.usersSubject.next(users.users.map((u) => ({
      userId: u.userId,
      username: u.username,
      avatarUrl: u.avatarUrl,
      isActive: false,
    })));
  }

  public async addChannel(channelName: string, channelPassword?: string) {
    await this.apiService.callJsonApi<API.ChannelAPI.request.CreateChannel, API.ChannelAPI.response.CreateChannel>(
      API.ChannelAPI.url.CreateChannel, {
        channelName,
        channelPassword
      }, this.userService.userInfo.accessToken);
  }

  public async joinChannel(channelId: number, channelPassword?: string) {
    await this.apiService.callJsonApi<API.ChannelAPI.request.JoinChannel, API.ChannelAPI.response.JoinChannel>(
      API.ChannelAPI.url.JoinChannel, {
        channelId,
        channelPassword,
      }, this.userService.userInfo.accessToken);
    this.currentJoinedChannel.next(this.channelsSubject.value.find((c) => c.channelId === channelId));
  }

  public async leaveCurrentChannel() {
    let channel = this.currentJoinedChannel.value;
    if (channel) {
      //-- empty message
      this.messagesSubject.next([]);
      await this.apiService.callJsonApi<API.ChannelAPI.request.LeaveChannel, API.ChannelAPI.response.LeaveChannel>(
        API.ChannelAPI.url.LeaveChannel, {
          channelId: channel.channelId,
        }, this.userService.userInfo.accessToken);
      this.currentJoinedChannel.next(undefined);
    }
  }

  public async loadMessageOfCurrentChannel(offset: number, amount: number) {
    let channel = this.currentJoinedChannel.value;
    if (channel) {
      this.messagesSubject.next([]);
      let messages = await this.apiService.callJsonApi<API.MessageAPI.request.GetMessageFromChannel, API.MessageAPI.response.GetMessageFromChannel>(
        API.MessageAPI.url.GetMessageFromChannel, {
          channelId: channel.channelId,
          offset,
          limit: amount,
        }, this.userService.userInfo.accessToken);
      this.messagesSubject.next(messages.messages.map((m) => ({
        messageId: m.messageId,
        content: m.content,
        channelId: m.channelId,
        senderId: m.senderId,
        timestamp: m.timestamp,
        messageType: m.contentType,
      })));
    }
  }

  public async loadJoinUserOfCurrentChannel() {
    let channel = this.currentJoinedChannel.value;
    if (channel) {
      let users = await this.apiService.callJsonApi<API.ChannelAPI.request.JoinedUsers, API.ChannelAPI.response.JoinedUsers>(
        API.ChannelAPI.url.JoinedUsers, {channelId: channel.channelId}, this.userService.userInfo.accessToken);
      this.currentChannelUsersSubject.next(users.userIds)
    }
  }

  public async updateChannelPassword(channelId: number, newPassword?: string) {
    await this.apiService.callJsonApi<API.ChannelAPI.request.UpdatePassword, API.ChannelAPI.response.UpdatePassword>(
      API.ChannelAPI.url.UpdatePassword, {
        channelId: channelId,
        password: newPassword
      }, this.userService.userInfo.accessToken);
  }

  public async removeChannel(channelId: number) {
    await this.apiService.callJsonApi<API.ChannelAPI.request.RemoveChannel, API.ChannelAPI.response.RemoveChannel>(
      API.ChannelAPI.url.RemoveChannel, {channelId: channelId}, this.userService.userInfo.accessToken);
  }
}
