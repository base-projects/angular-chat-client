export interface Channel {
  channelId: number,
  channelName: string;
  isPassword: boolean;
  creatorId?: number;
}
