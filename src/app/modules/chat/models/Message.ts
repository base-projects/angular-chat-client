import * as API from "../../../../../../claire-cli/lib/template/export-1.0.0";

export interface Message {
  content: string;
  messageType: API.MessageType;
  messageId?: string;
  channelId?: number;
  senderId?: number;
  timestamp?: number;
}
