export interface User {
  userId: number;
  username: string;
  avatarUrl: string;
  isActive: boolean;
}
