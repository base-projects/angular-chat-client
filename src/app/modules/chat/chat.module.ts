import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {routes} from "./chat.routing";
import {HomePage} from "./pages/home/home.page";
import {SocketService} from "./services/SocketService";
import {UserListPartial} from "./pages/home/partials/userlist/user-list.partial";
import {ChannelListPartial} from "./pages/home/partials/channel-list/channel-list.partial";
import {PMPartial} from "./pages/home/partials/pm/pm.partial";
import {ChatBulletComponent} from "./components/chat-bullet/chat-bullet.component";
import {CommonModule} from "@angular/common";
import {MessageInputComponent} from "./components/message-input/message-input.component";
import {FormsModule} from "@angular/forms";
import {ChatUserService} from "./services/ChatUserService";
import {UserBulletComponent} from "./components/user-bullet/user-bullet.component";
import {UIModule} from "../ui/ui.module";
import {ChannelBulletComponent} from "./components/channel-bullet/channel-bullet.component";
import {NgbDropdownModule, NgbTooltipModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    HomePage,
    UserListPartial,
    ChannelListPartial,
    PMPartial,
    ChatBulletComponent,
    MessageInputComponent,
    UserBulletComponent,
    ChannelBulletComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    UIModule,
    NgbTooltipModule,
    NgbDropdownModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    SocketService,
    ChatUserService,
  ]
})
export class ChatModule {

}
