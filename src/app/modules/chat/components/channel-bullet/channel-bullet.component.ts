import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: "channel-bullet",
  templateUrl: "./channel-bullet.component.html",
})
export class ChannelBulletComponent{

  @Input()
  showJoinButton: boolean;

  @Input()
  channelName: string;

  @Input()
  isPassword: boolean;

  @Input()
  ownerAvatar?: string;

  @Input()
  ownerName?: string;

  @Output()
  onJoin: EventEmitter<any> = new EventEmitter();

  joinChannel(){
    this.onJoin.emit();
  }

}
