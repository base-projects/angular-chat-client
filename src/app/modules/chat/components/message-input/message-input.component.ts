import {MessageType} from "../../../../../../../claire-cli/lib/template/export-1.0.0";
import {Component, EventEmitter, Output} from "@angular/core";
import {AppError, ErrorHandlerService} from "../../../../services/ErrorHandlerService";
import {Message} from "../../models/Message";

@Component({
  selector: "message-input",
  templateUrl: "./message-input.component.html",
})
export class MessageInputComponent {

  public constructor(private errorHandlerService: ErrorHandlerService) {

  }

  message: string;

  @Output()
  onSend: EventEmitter<Message> = new EventEmitter();

  sendMessage(files: File[]) {
    if (files && files.length) {
      let file = files[0];
      if (file.size > 512000) {
        this.errorHandlerService.handle(new AppError("FILE_SIZE_LIMIT", "Please choose an image less than 512KB"));
      } else {
        //-- serialize the image as content
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.onSend.emit({content: String(reader.result), messageType: MessageType.IMAGE});
        };
        reader.onerror = () => {
          this.errorHandlerService.handle(new AppError("FILE_READ_ERROR", ""));
        };
      }
    } else {
      this.onSend.emit({content: this.message, messageType: MessageType.TEXT});
      this.message = "";
    }
  }

}
