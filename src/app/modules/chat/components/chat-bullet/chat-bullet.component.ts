import {Component, Input} from "@angular/core";
import {MessageType} from '../../../../../../../claire-cli/lib/template/export-1.0.0';

@Component({
  selector: "chat-bullet",
  templateUrl: "./chat-bullet.component.html",
  styleUrls: ["./chat-bullet.component.scss"],
})
export class ChatBulletComponent {

  @Input()
  username: string;

  @Input()
  timestamp: string;

  @Input()
  message: string;

  @Input()
  userAvatar: string;

  @Input()
  messageType: MessageType;

}
