import {Component, Input} from "@angular/core";

@Component({
  selector: "user-bullet",
  templateUrl: "./user-bullet.component.html",
})
export class UserBulletComponent{

  @Input()
  isActive: boolean;

  @Input()
  username: string;

  @Input()
  avatarUrl: string;

}
