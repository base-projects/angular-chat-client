import {Routes} from "@angular/router";
import {HomePage} from "./pages/home/home.page";

export const routes: Routes = [
  {
    path: "", children: [
      {path: "", pathMatch: "full", redirectTo: "home"},
      {path: "home", component: HomePage},
    ]
  }
];
