import {Routes,} from "@angular/router";
import {AuthGuardProvider} from "./services/AuthGuardProvider";
import {NotFoundPage} from "./pages/404/404.page";
import {AccessDeniedPage} from "./pages/403/403.page";

export const routes: Routes = [
  {path: "", redirectTo: "auth", pathMatch: "full"},
  {path: "auth", loadChildren: "./modules/auth/auth.module#AuthModule"},
  {path: "chat", loadChildren: "./modules/chat/chat.module#ChatModule", canActivate: [AuthGuardProvider]},
  {path: "403", component: AccessDeniedPage},
  {path: "404", component: NotFoundPage},
  {path: "**", redirectTo: "404"}
];
