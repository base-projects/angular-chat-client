import {FormsModule} from '@angular/forms';
import {RouterModule} from "@angular/router";
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {App} from './app';
import {routes} from "./app.routing";
import {AccessDeniedPage} from "./pages/403/403.page";
import {NotFoundPage} from "./pages/404/404.page";
import {ApiService} from "./services/ApiService";
import {AuthGuardProvider} from "./services/AuthGuardProvider";
import {ErrorHandlerService} from "./services/ErrorHandlerService";
import {UserService} from "./services/UserService";
import {UIModule} from "./modules/ui/ui.module";

@NgModule({
  declarations: [
    App,
    AccessDeniedPage,
    NotFoundPage,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    UIModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    AuthGuardProvider,
    ApiService,
    ErrorHandlerService,
    UserService,
  ],
  bootstrap: [App]
})
export class AppModule {
}
