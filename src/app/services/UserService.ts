import * as API from "../../../../claire-cli/lib/template/export-1.0.0";
import {Injectable} from "@angular/core";
import {ApiService} from "./ApiService";
import {Router} from "@angular/router";
import {ErrorHandlerService} from "./ErrorHandlerService";

@Injectable()
export class UserService {

  private static readonly LOCAL_STORAGE_TOKEN = "user_info";

  public userInfo: {
    userId?: number,
    username?: string,
    avatarUrl?: string,
    accessToken?: string,
  };

  public constructor(private apiService: ApiService, private router: Router, private errorHandlerService: ErrorHandlerService) {
    let data = localStorage.getItem(UserService.LOCAL_STORAGE_TOKEN);
    this.userInfo = data ? JSON.parse(data) : {};
  }

  private saveInfoToLocalStorage() {
    localStorage.setItem(UserService.LOCAL_STORAGE_TOKEN, JSON.stringify(this.userInfo));
  }

  private clearInfoFromLocalStorage() {
    localStorage.removeItem(UserService.LOCAL_STORAGE_TOKEN);
  }

  public logout() {
    //-- clear user info
    this.userInfo = {};
    this.clearInfoFromLocalStorage();
    this.router.navigate(["/auth"]);
  }

  public async updateUserAvatar(file) {
    let formData = new FormData();
    formData.append("avatar", file);
    await this.apiService.callJsonApi("/users/update_avatar", formData, this.userInfo.accessToken);
  }

  public updateUserLoginInfo(accessToken: string): void {
    this.userInfo.accessToken = accessToken;
    this.saveInfoToLocalStorage();
  }

  public updateUserBasicInfo(userId?: number, username?: string, avatarUrl?: string): void {
    if (userId !== undefined) {
      this.userInfo.userId = userId;
    }
    if (username !== undefined) {
      this.userInfo.username = username;
    }
    if (avatarUrl !== undefined) {
      this.userInfo.avatarUrl = avatarUrl;
    }
    this.saveInfoToLocalStorage();
  }

  public async getMyInfo(): Promise<API.UserAPI.response.GetUser> {
    let infoResult = await this.apiService.callJsonApi<undefined, API.UserAPI.response.GetUser>(
      API.UserAPI.url.GetUser.replace(":id", String(this.userInfo.userId)), undefined, this.userInfo.accessToken);
    return infoResult;
  }

}
