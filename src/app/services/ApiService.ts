import axios from "axios";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {AppError, ErrorHandlerService} from "./ErrorHandlerService";

@Injectable()
export class ApiService {


  public constructor(private errorHandlerService: ErrorHandlerService) {
  }

  public async callJsonApi<T, V>(url: string, data?: T, token?: string): Promise<V> {
    let headers: any = {};

    if (token) {
      headers = Object.assign(headers, {"Authorization": token});
    }

    let result;
    if (data) {
      if (data instanceof FormData) {
        headers = Object.assign(headers, {"Content-Type": "multipart/form-data"});
      } else {
        headers = Object.assign(headers, {"Content-Type": "application/json"});
      }
      result = await axios.post(environment.API_SERVER_URL + url, data, {headers});
    } else {
      result = await axios.get(environment.API_SERVER_URL + url, {method: "GET", headers});
    }
    if (!result["data"]) {
      throw new AppError("NETWORK_ERROR", "");
    }
    if (!result["data"]["success"]) {
      //-- check if error is TOKEN_EXPIRED then call the errorHandlerService
      throw new AppError(result["data"]["result"]["name"], result["data"]["result"]["message"]);
    }
    return result["data"]["result"] as V;
  }
}
