import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Injectable()
export class ErrorHandlerService {

  public lastError: BehaviorSubject<AppError | undefined>;

  public constructor() {
    this.lastError = new BehaviorSubject<AppError | undefined>(undefined);
  }

  public handle(err: AppError): void {
    this.lastError.next(err);
  }

}

export class AppError {

  public name: string;
  public message: string;

  constructor(name: string, message: string){
    this.name = name;
    this.message = message;
  }

}
