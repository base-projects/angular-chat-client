import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {UserService} from "./UserService";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthGuardProvider implements CanActivate {

  public constructor(private router: Router, private userService: UserService) {

  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.userService.userInfo.accessToken) {
      this.router.navigate(["/403"]);
      return false;
    }
    return true;
  }
}
